/*
 * This file is part of Epic Tweaks.
 * Copyright (c) 2024, HDainester, All rights reserved.
 *
 * Epic Tweaks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Epic Tweaks is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along
 * with Epic Tweaks. If not, see <http://www.gnu.org/licenses/lgpl>.
 */
package com.gitlab.srcmc.epictweaks.forge.mixins.epicfight.client.events.engine;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import com.gitlab.srcmc.epictweaks.forge.ModForge;
import com.gitlab.srcmc.epictweaks.forge.client.events.engine.IRenderEngine;

import yesman.epicfight.client.events.engine.RenderEngine;
import yesman.epicfight.client.renderer.FirstPersonRenderer;

@Mixin(RenderEngine.class)
public class RenderEngineMixin implements IRenderEngine {
    @Shadow
    private FirstPersonRenderer firstPersonRenderer;

    @Override
    public FirstPersonRenderer getFirstPersonRenderer() {
        return firstPersonRenderer;
    }

    @Inject(method = "<init>", at = @At("RETURN"), remap = false)
    public void onConstructed(CallbackInfo ci) {
        ModForge.RENDER_ENGINE = (RenderEngine)(Object)this;
    }
}