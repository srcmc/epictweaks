/*
 * This file is part of Epic Tweaks.
 * Copyright (c) 2024, HDainester, All rights reserved.
 *
 * Epic Tweaks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Epic Tweaks is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along
 * with Epic Tweaks. If not, see <http://www.gnu.org/licenses/lgpl>.
 */
package com.gitlab.srcmc.epictweaks.forge.mixins.epicfight.client.events.engine;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import com.gitlab.srcmc.epictweaks.forge.ModForge;
import com.gitlab.srcmc.epictweaks.forge.client.events.engine.IRenderEngine;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.LivingEntityRenderer;
import net.minecraft.world.InteractionHand;
import net.minecraftforge.client.event.RenderHandEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import yesman.epicfight.client.ClientEngine;
import yesman.epicfight.client.events.engine.RenderEngine;
import yesman.epicfight.client.world.capabilites.entitypatch.player.LocalPlayerPatch;
import yesman.epicfight.main.EpicFightMod;

@Mixin(RenderEngine.Events.class)
public class RenderEngineEventsMixin {
    @Overwrite
    @SubscribeEvent
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public static void renderHand(RenderHandEvent event) {
        LocalPlayerPatch playerpatch = ClientEngine.getInstance().getPlayerPatch();
        
        if(playerpatch != null) {
            boolean isBattleMode = playerpatch.isBattleMode();

            if(isBattleMode || (!EpicFightMod.CLIENT_INGAME_CONFIG.filterAnimation.getValue() && !ModForge.CLIENT_CONFIG.getFilterAnimationFirstPerson())) {
                if (event.getHand() == InteractionHand.MAIN_HAND) {
                    ((IRenderEngine)ModForge.RENDER_ENGINE).getFirstPersonRenderer().
                        render(playerpatch.getOriginal(), playerpatch,
                        (LivingEntityRenderer)Minecraft.getInstance().getEntityRenderDispatcher().getRenderer(playerpatch.getOriginal()),
                        event.getMultiBufferSource(), event.getPoseStack(), event.getPackedLight(), event.getPartialTick());
                }
                
                event.setCanceled(true);
            }
        }
    }
}