/*
 * This file is part of Epic Tweaks.
 * Copyright (c) 2024, HDainester, All rights reserved.
 *
 * Epic Tweaks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Epic Tweaks is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along
 * with Epic Tweaks. If not, see <http://www.gnu.org/licenses/lgpl>.
 */
package com.gitlab.srcmc.epictweaks.forge.config;

import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.common.ForgeConfigSpec.ConfigValue;

public class ClientConfig {
    private final ConfigValue<Boolean> filterAnimationFirstPerson;
    private final ConfigValue<Boolean> enforceMode;
    private final ForgeConfigSpec spec;

    public ClientConfig() {
        this(new ForgeConfigSpec.Builder());
    }

    public ClientConfig(ForgeConfigSpec.Builder builder) {
        filterAnimationFirstPerson = builder
            .comment("\nThis value only has an effect if Epic Fights 'filter_animation' is disabled.",
                "Set this to 'true' to prevent Epic Fights rendering in first person view while in mining mode.",
                "\nDefault: true")
            .define("filter_animation_first_person", true);

        enforceMode = builder
            .comment("\nIf enabled the player cannot manually switch between battle or mining mode if the item in his hand is designated to either of the modes",
                "(i.e. the item was added to Epic Fights 'battle_autoswitch_items' or 'mining_autoswitch_items').",
                "\nDefault: true")
            .define("enforce_mode", true);

        spec = builder.build();
    }

    public ForgeConfigSpec getSpec() {
        return spec;
    }

    public boolean getFilterAnimationFirstPerson() {
        return filterAnimationFirstPerson.get();
    }

    public boolean getEnforceMode() {
        return enforceMode.get();
    }
}