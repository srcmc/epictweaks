/*
 * This file is part of Epic Tweaks.
 * Copyright (c) 2024, HDainester, All rights reserved.
 *
 * Epic Tweaks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Epic Tweaks is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along
 * with Epic Tweaks. If not, see <http://www.gnu.org/licenses/lgpl>.
 */
package com.gitlab.srcmc.epictweaks.forge.mixins.epicfight.world.capabilities.entitypatch.player;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.spongepowered.asm.mixin.Shadow;

import com.gitlab.srcmc.epictweaks.forge.ModForge;

import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Items;
import yesman.epicfight.main.EpicFightMod;
import yesman.epicfight.world.capabilities.entitypatch.LivingEntityPatch;
import yesman.epicfight.world.capabilities.entitypatch.player.PlayerPatch;
import yesman.epicfight.world.capabilities.entitypatch.player.PlayerPatch.PlayerMode;

@Mixin(PlayerPatch.class)
public abstract class PlayerPatchMixin<T extends Player> extends LivingEntityPatch<T> {
    @Shadow
    protected PlayerMode playerMode;

    @Shadow
    public void toBattleMode(boolean synchronize) {}

    @Shadow
    public void toMiningMode(boolean synchronize) {}

    @Overwrite
    public void toggleMode() {
		var stackInHand = getOriginal().getMainHandItem();

		switch (this.playerMode) {
		case MINING:
			if(!ModForge.CLIENT_CONFIG.getEnforceMode()
				|| EpicFightMod.CLIENT_INGAME_CONFIG.battleAutoSwitchItems.contains(stackInHand.getItem())
				|| !EpicFightMod.CLIENT_INGAME_CONFIG.miningAutoSwitchItems.contains(stackInHand.getItem())
				|| stackInHand.is(Items.AIR))
			{
				this.toBattleMode(true);
			}
			break;
		case BATTLE:
			if(!ModForge.CLIENT_CONFIG.getEnforceMode()
				|| EpicFightMod.CLIENT_INGAME_CONFIG.miningAutoSwitchItems.contains(stackInHand.getItem())
				|| !EpicFightMod.CLIENT_INGAME_CONFIG.battleAutoSwitchItems.contains(stackInHand.getItem())
				|| stackInHand.is(Items.AIR))
			{
				this.toMiningMode(true);
			}
			break;
		}
    }
}