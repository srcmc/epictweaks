/*
 * This file is part of Epic Tweaks.
 * Copyright (c) 2024, HDainester, All rights reserved.
 *
 * Epic Tweaks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Epic Tweaks is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along
 * with Epic Tweaks. If not, see <http://www.gnu.org/licenses/lgpl>.
 */
package com.gitlab.srcmc.epictweaks.forge;

import com.gitlab.srcmc.epictweaks.ModCommon;
import com.gitlab.srcmc.epictweaks.forge.config.ClientConfig;

import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.config.ModConfig;
import yesman.epicfight.client.events.engine.RenderEngine;

@Mod(ModCommon.MOD_ID)
public class ModForge {
    public static RenderEngine RENDER_ENGINE;
    public static ClientConfig CLIENT_CONFIG;

    static { ModRegistries.init(); }

    public ModForge() {
        CLIENT_CONFIG = new ClientConfig();
        ModLoadingContext.get().registerConfig(ModConfig.Type.CLIENT, CLIENT_CONFIG.getSpec());        
    }
}