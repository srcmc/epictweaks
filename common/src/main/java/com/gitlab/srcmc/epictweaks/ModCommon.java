/*
 * This file is part of Epic Tweaks.
 * Copyright (c) 2024, HDainester, All rights reserved.
 *
 * Epic Tweaks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Epic Tweaks is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along
 * with Epic Tweaks. If not, see <http://www.gnu.org/licenses/lgpl>.
 */
package com.gitlab.srcmc.epictweaks;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ModCommon {
    public static final String MOD_ID = "epictweaks"; // must match mod_id
    public static final String MOD_NAME = "Epic Tweaks"; // should match mod_name
    public static final Logger LOG = LoggerFactory.getLogger(MOD_NAME);
}