# Changelog

## [1.0.2] - 2024-02-26

***Fixed***

- Minor: internal translation bug

## [1.0.1] - 2024-02-26

***Fixed***

- Missing dependency notice for EpicFight version (now gives a reasonable error if version is not supported)

## [1.0.0] - 2024-02-12

***Added***

- 'enforce_mode' config option
- 'filter_animation_first_person' config option
